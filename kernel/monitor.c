#include <stddef.h> /* For size_t and NULL */
#include <stdint.h> /* For intx_t uint_t datatypes */

#include "string.h"
#include "common.h"
#include "monitor.h"

static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 25;


static uint8_t make_color(enum vga_color fg, enum vga_color bg) {
    return fg | bg << 4;
}

static uint16_t make_vgaentry(char c, uint8_t color) {
    uint16_t c16 = c;
    uint16_t color16 = color;
    return c16 | color16 << 8;
}

void terminal_init(void) {
    terminal_row = 0;
    terminal_column = 0;
    terminal_color = make_color(COLOR_LIGHT_GREY, COLOR_BLACK);
    terminal_buffer = (uint16_t*) 0xB8000; /* Text screen video memory adr*/
    
    size_t y, x;
    for (y = 0; y < VGA_HEIGHT; y++) {
        for (x = 0; x < VGA_WIDTH; x++) {

            const size_t index = y * VGA_WIDTH + x;
            terminal_buffer[index] = make_vgaentry(' ', terminal_color);
        }
    }
}

void terminal_setcolor(uint8_t color) {
    terminal_color = color; 
}

static void terminal_putentryat(char c, uint8_t color, size_t x, size_t y) {
    const size_t index = y * VGA_WIDTH + x;
    terminal_buffer[index] = make_vgaentry(c, color);
}

static void terminal_scroll(void) {

    if (terminal_row >= 25) {
        /* Move current text in buffer by a line */
        unsigned tmp = terminal_row - 25 + 1;
        memcpy(terminal_buffer, terminal_buffer + tmp * 80, (25 - tmp) * 80 * 2);

        /* Clear last line of text */
        size_t x;
        for (x = 0; x < VGA_WIDTH; x++) {
            terminal_putentryat(' ', terminal_color, x, 24);
        }
        terminal_row  = 24;
    }
}

static void move_cursor(void) {
    /* Screen is max 80 characters wide */
    uint16_t cursorLoc = terminal_row * 80 + terminal_column;
    
    outb(0x3D4, 14);    /* Notify vga board, high cursor byte */ 
    outb(0x3D5, cursorLoc >> 8);    /* Send high curcor byte*/
    outb(0x3D4, 15);    /* Notify vga board, low cursor byte */
    outb(0x3D5, cursorLoc);
}   

static void terminal_putchar(char c) {
    terminal_putentryat(c, terminal_color, terminal_column, terminal_row);
    if (++terminal_column == VGA_WIDTH) {
        terminal_column = 0;
        if (++terminal_row == VGA_HEIGHT) {
            terminal_scroll();
        }
    }
    move_cursor();
}

void terminal_writestring(const char* data) {

    size_t i;
    size_t datalen = strlen(data);
    
    for (i = 0; i < datalen; i++) {
        terminal_putchar(data[i]);
    }
}

void terminal_clr(void) {

    uint16_t blank = 0x20 | (terminal_color << 8);

    uint16_t i = 0;
    for (i = 0; i < 25*80; i++) {
        terminal_buffer[i] = blank;
    }
    terminal_row = 0;
    terminal_column = 0;
    move_cursor();
}