#include <stddef.h>
#include <stdint.h>

#include "gdt.h"

/* Setup a gdt entry */
static void gdt_set_gate(uint32_t num, uint32_t base, uint32_t limit, uint8_t access, uint8_t gran) {

	gdt_entry[num].base_low = (base & 0xFFFF);
	gdt_entry[num].base_middle = (base >> 16) & 0xFF;
	gdt_entry[num].base_high = (base >> 24) & 0xFF;

	gdt_entry[num].limit_low = (limit & 0xFFFF);
	gdt_entry[num].granularity = (limit >> 16) & 0x0F;

	gdt_entry[num].granularity |= gran & 0xF0;
	gdt_entry[num].access = access;
}

/* Called by kernel main. This sets up the special GDT 
 * pointer. Tells processor where new GDT is and update
 * the new segment registers 
 */
void gdt_init(void) {
	gdt_ptr.limit = (sizeof(gdt_entry_t) * 5) - 1;
	gdt_ptr.base = (uint32_t) &gdt_entry;

	gdt_set_gate(0, 0, 0, 0, 0);				// Null segment
	gdt_set_gate(1, 0, 0xFFFFFFFF, 0x9A, 0xCF);	// Code segment
	gdt_set_gate(2, 0, 0xFFFFFFFF, 0x92, 0xCF);	// Data segment
	gdt_set_gate(3, 0, 0xFFFFFFFF, 0xFA, 0xCF);	// User mode code segment
	gdt_set_gate(4, 0, 0xFFFFFFFF, 0xF2, 0xCF);	// User mode data segment

	/* ASM function to reload new segment registers */
	gdt_flush((uint32_t) &gdt_ptr);
}


