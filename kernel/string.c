#include <stddef.h> /* For sizes_t and NULL */
#include <stdint.h> /* For intx_t uint_t datatypes */

#include "string.h"

/* Copy contents pointed to by src of at most size 
 * length, to location pointed to by dst pointer 
 */
void *memcpy(void *dst, const void *src, size_t size) {
	uint8_t *pdst = (uint8_t*) dst;
	uint8_t *psrc = (uint8_t*) src;

	size_t loops = (size / sizeof(uint32_t));
	size_t index = 0;
	for(index = 0; index < loops; ++index) {

		*((uint32_t*) pdst) = *((uint32_t*) psrc);
		pdst += sizeof(uint32_t);
		psrc += sizeof(uint32_t);
	}

	loops = (size % sizeof(uint32_t) );
	for (index = 0; index < loops; ++index) {

		*pdst = *psrc;
		++pdst;
		++psrc;
	}

	return pdst;
}

/* Copy the (unsigned char) c to the first len characters
 * of the string pointed to by str pointer 
 */
void *memset(void *str, int c, size_t len) {

	unsigned char *dst = str;
	while (len--) {
		*dst = (unsigned char) c;
		dst++;
		len--;
	}
	return str;
}

/* Compare first len bytes of memory location s1 and s2
 * Each n bytes interpreted at unsigned char
 */
int memcmp(const void *s1, const void *s2, size_t len) {

	const unsigned char *c1 = s1, *c2 = s2;
	while (len--) {
		if(*c1 != *c2) {
			return *c1 - *c2;
		} else {
			c1++;
			c2++;
			len--;
		}
	}
	return 0;
}

/* Return length of str */
size_t strlen(const char* str) {
    size_t ret = 0;
    while (str[ret] != 0) {
        ret++;    
    }
    return ret;
}