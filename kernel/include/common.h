#ifndef COMMON_H
#define COMMON_H

/* These commands write to CRT Control Register
 * of the VGA controller. They are written as 
 * separate high and low bytes 
 */
 
/* Write to port byte value */
void outb(uint16_t port, uint8_t value);
/* Read from port 8bits */
uint8_t inb(uint16_t port);
/* Read from port 16bits */
uint16_t inw(uint16_t port);

#endif // COMMON_H

