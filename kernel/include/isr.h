#ifndef __ISR_H
#define __ISR_H

typedef struct regs_struct 
{
	uint32_t gs, fs, es, ds;	/* pushed the segs last */
	uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax;	/* From pusha */
	uint32_t int_no, err_code;		/* Interpt number and err code */
	uint32_t eip, cs, eflags, useresp, ss;	/* Pushed by processor automatically */
} regs_t;

/* Handler for Interrupt Service Routines (ISRs) 
   This gets called from ASM isr_common_stub*/
void isr_handler(regs_t);

#endif // ISR_H