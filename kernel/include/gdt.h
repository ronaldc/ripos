#ifndef GDT_H
#define GDT_H

/* The GDT defines base access privilages for memory. It contains
 * 64-bit long entries defining where in memory it starts and access
 * associated with it. Entry 0 is known as NULL descriptor; access will
 * trigger General Protection fault. Modern OS use paging which is more
 * versatile with higher flexibility.
 *
 * If loaded from ex. GRUB, an GDT exists; triple-fault if we attempt
 * to overwrite this location. Thus we create our own and tell process
 * the location of it using 'lgdt' asm code.  
 *
 *	x86 segment registers (6): CS, DS, ES, FS, GS, SS
 */

/* The structure of a gdt entry (48 bits + acces + granularity)
 * --Prevent gcc from optimizing 
 */
typedef struct gdt_entry_struct
{
	uint16_t limit_low;		// Lower 16 bits of limit
	uint16_t base_low;		// Lower 16 bits of base
	uint8_t base_middle; 	// Next 8 bits of base
	uint8_t access;			// Access flags
	uint8_t granularity;	
	uint8_t	base_high;		// Last 8 bits of base
} __attribute__((packed)) gdt_entry_t;

/* The gdt pointer structure 
 * --Prevent gcc from optimizing 
 */
typedef struct gdt_ptr_struct
{
	uint16_t limit;		// Upper 16 bits of all selector limits
	uint32_t base;		// Address of first gdt entry struct
} __attribute__((packed)) gdt_ptr_t;


gdt_entry_t gdt_entry[5];
gdt_ptr_t gdt_ptr;


/* Initialize GDT */
void gdt_init(void);
/* ASM directive to reload gdt */
extern void gdt_flush(uint32_t);

#endif // GDT_H