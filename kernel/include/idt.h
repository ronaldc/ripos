#ifndef IDT_H
#define IDT_H

/* Interrupt Descriptor Table (IDT) is a data structure containing entries to
 * interrupt service routines (ISRs) to handle an interrupt. These entries may 
 * also be called by interrupt requests. Similar to GDT entries, they are 
 * 64bits in length. Differences include, base addr which defines the ISR. Also
 * an IDT entry has no limit, but a kernel segment which needs to be specified.
 * Segment must be the same segment given ISR is located in.
 *
 * Access flags are also different but closely similar. */

/* Defines an IDT entry */
typedef struct idt_entry_struct
{	// Base address refers to ISR
	uint16_t base_low;	// Lower 16 bits of base
	uint16_t sel;		// Kernel segment selector
	uint8_t always0;	// Must be 0
	uint8_t flags;		// More flags.
	uint16_t base_high;	// Upper 16 bits of base adr
} __attribute__((packed)) idt_entry_t;

/* The idt pointer structure to an array of interrupt handlers */
typedef struct idt_ptr_struct
{
	uint16_t limit;
	uint32_t base;	// Adr of first element in idt_entry_t array
} __attribute__((packed)) idt_ptr_t;

idt_entry_t idt_entry[256];
idt_ptr_t idt_ptr;

/* Initialize IDT */
void idt_init(void);
/* ASM Directive to load idt */
extern void idt_flush(uint32_t);

/* Directives to access ASM ISR handlers */
// Interrupt Service Routines (ISRs)
extern void isr0();
extern void isr1();
extern void isr2();
extern void isr3();
extern void isr4();
extern void isr5();
extern void isr6();
extern void isr7();
extern void isr8();
extern void isr9();
extern void isr10();
extern void isr11();
extern void isr12();
extern void isr13();
extern void isr14();
extern void isr15();
extern void isr16();
extern void isr17();
extern void isr18();
extern void isr19();
extern void isr20();
extern void isr21();
extern void isr22();
extern void isr23();
extern void isr24();
extern void isr25();
extern void isr26();
extern void isr27();
extern void isr28();
extern void isr29();
extern void isr30();
extern void isr31();
// Interrupt Request (IRQs)
extern void irq0();
extern void irq1();
extern void irq2();
extern void irq3();
extern void irq4();
extern void irq5();
extern void irq6();
extern void irq7();
extern void irq8();
extern void irq9();
extern void irq10();
extern void irq11();
extern void irq12();
extern void irq13();
extern void irq14();
extern void irq15();

#endif // IDT_H
