#ifndef MONITOR_H
#define MONITOR_H

#include <stddef.h>

/* Hardware text mode color constants 
 * Lowest 4bits foreground; high 3bits background 
 * Interpretation of bit #7 depends on hardware config
 *  15 - Background - 12 11 - Forecolor - 8 7 - Char - 0
 */
enum vga_color {
    COLOR_BLACK = 0,
    COLOR_BLUE = 1,
    COLOR_GREEN = 2,
    COLOR_CYAN = 3,
    COLOR_RED = 4,
    COLOR_MAGENTA = 5,
    COLOR_BROWN = 6,
    COLOR_LIGHT_GREY = 7,
    COLOR_DARK_GREY = 8,
    COLOR_LIGHT_BLUE = 9,
    COLOR_LIGHT_GREEN = 10,
    COLOR_LIGHT_CYAN = 11,
    COLOR_LIGHT_RED = 12,
    COLOR_LIGHT_MAGENTA = 13,
    COLOR_LIGHT_BROWN = 14,
    COLOR_WHITE = 15,
};

size_t terminal_row;
size_t terminal_column;
uint8_t terminal_color;
uint16_t* terminal_buffer;

/* Start terminal with default color */
void terminal_init(void);
/* Change terminal default color */
void terminal_setcolor(uint8_t color);
/* Write a string to terminal */
void terminal_writestring(const char*);
/* Clear terminal */
void terminal_clear(void);

#endif // MONITOR_H