#include <stdint.h>

#include "common.h"
#include "irq.h"
#include "isr.h"

void irq_handler(regs_t regs) {
	// Send EOI signal to PICs
	if (regs.int_no >= 40) {
		// Slave reset signal
		outb(PIC2_COMMAND, 0x20);
	}
	// Send reset signal to master 
	outb(PIC1_COMMAND, 0x20);


}