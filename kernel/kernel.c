/* kernel.o
 *
 * Compile with:
 * i686-elf-gcc -c kernel.c -o kernel.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
 *
 */
#if !defined(__Cplusplus)
#include <stdbool.h> /* C doesn't have booleans by default */
#endif

#include <stddef.h> /* For size_t and NULL */
#include <stdint.h> /* For intx_t uint_t datatypes */

#include "monitor.h"    
#include "gdt.h"    /* Global Descriptor Table */
#include "idt.h"    /* Interrupt Descriptor Table */
#include "isr.h"    /* Interrupt Service Routines */

/* Check if the complier thinks we are targeting the wrong OS */
#if defined(__linux__)
#error "Not ran as cross-compiler"
#endif

/* This tutorial will only work for the 32-bit ix86 targets */
#if !defined(__i386__)
#error "Requires ix86 compiler"
#endif

#if defined(__cplusplus)
extern "C" /* Use C linkage for kernel_main */
#endif

void kernel_main() {
    /* Initialize terminal interface */
    terminal_init();
    gdt_init();
    idt_init();

    /* No support for newlines in terminal_putchar 
     * yet '\n' will produce VGA specific char instead
     */ 
    asm volatile ("int $0x3");
    terminal_writestring("Kernel mode");

    /* Kernel hang */
    for(;;);
}

