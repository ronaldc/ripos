# ripOS.bin makefile
AS = nasm
CC = i686-elf-gcc
CLFAGS = -std=gnu99 -ffreestanding -O2 -Wall -Wextra

SRCDIR := kernel
HDRDIR := kernel/include
UTILDIR := util
BOOTDIR := boot

UTILFILE := $(wildcard $(UTILDIR)/*.ld)
OBJBOOT := $(patsubst %.asm, %.o, $(wildcard $(BOOTDIR)/*.asm))
SRCFILES := $(wildcard $(SRCDIR)/*.c)
OBJFILES := $(patsubst %.c, %.o, $(SRCFILES))

ripOS.bin: $(OBJFILES) $(OBJBOOT)
	$(CC) -T $(UTILFILE) -o $@ -ffreestanding -O2 -nostdlib $^ -lgcc

$(SRCDIR)/%.o: $(SRCDIR)/%.c
	$(CC) -c -I./$(HDRDIR) $(CLFAGS) -o $@ $<

$(BOOTDIR)/%.o: $(BOOTDIR)/%.asm
	$(AS) -f elf32 $^ -o $@

.PHONY: clean
clean:
	rm -f $(OBJFILES) $(OBJBOOT)